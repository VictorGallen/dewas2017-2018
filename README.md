# README #

### What is this repository for? ###

This repo consists of my work in Development of Web Applications and Services course during the fall of 2018.
Our assignment is to create a "YAAS" website. (Yet Another Auction Site)
The website should allow a user to signup, change email/password, login/logout. Create auctions, bid on auctions etc.etc.
Basically a fully working Auction site.

The Deadline for the project is 31.4, so by then you can expect this Project to be working fully and available for cloning!

### How do I get set up? ###

I am using the Python Web framework Django to complete the tasks in this course, I am using Django as my IDE.
For now there is no further instructions on how to get this code working on your local system since the project is still in it's early phase.

### Who do I talk to? ###

Contact me @ gallen.victor@gmail.com if you have any questions!
Happy coding :)